package com.example.todo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Switch
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private var editTextTarefa: EditText? = null
    private var txtTarefa: TextView? = null
    private var switch1: Switch? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.txtTarefa  = findViewById(R.id.txtTarefas)
        this.editTextTarefa  = findViewById(R.id.editTextTarefa)
        this.switch1 = findViewById(R.id.switch1)
    }

    fun onClickOk(v: View) {
        val tarefa: String = this.editTextTarefa?.text.toString()
        if(tarefa.isNotEmpty()){
            if(switch1?.isChecked == true){
                this.txtTarefa?.text = this.txtTarefa?.text.toString() + "(Urgent) " + tarefa + "\n"
                this.editTextTarefa?.setText("")
            }else{
                this.txtTarefa?.text = this.txtTarefa?.text.toString() + "(Not Urgent) " + tarefa + "\n"
                this.editTextTarefa?.setText("")
            }
        }
    }
}